var giraffe = require('../../src/js/giraffe.js');

var chai = require('chai');
var should = chai.should();

describe('Zoo', function() {

  describe('Giraffe', function() {
    it('should speak like a giraffe', function() {
      giraffe.speak().should.equal("Hello. I'm a Giraffe");
    });
  });
});