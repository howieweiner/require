//var assert = require("assert");
describe('Array', function() {
  describe('#indexOf()', function () {
    it('should return -1 when the value is not present', function () {
      assert.equal(-1, [1,2,3].indexOf(5));
      assert.equal(-1, [1,2,3].indexOf(0));
    });
  });
});

//require.config({
//  paths: {
//    'mocha': '../node_modules/mocha/mocha.js',
//    'chai': '../node_modules/chai/chai.js',
//    'giraffe': '../src/js/giraffe.js'
//  }
//});
//
//define(function (require) {
//  var mocha = require('mocha');
//  var chai = require('chai');
//
//  var should = chai.should();
//
//  mocha.setup('bdd');
//
//  require([
//    './spec/zoo-spec.js'
//  ], function (require) {
//    if (window.mochaPhantomJS) {
//      mochaPhantomJS.run();
//    }
//    else {
//      mocha.run();
//    }
//  });
//});