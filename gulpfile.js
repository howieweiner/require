// https://github.com/karma-runner/gulp-karma/blob/master/gulpfile.js
var gulp = require('gulp');
var karma = require('karma').server;

/**
 * Run test once and exit
 */

gulp.task('test', function(done) {
    karma.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, function() {
        done();
    });
});
