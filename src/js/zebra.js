define(function() {
 'use strict';

  var speak = function() {
    return "Hello. I'm a Zebra";
  };

  return {
    speak: speak
  }
});