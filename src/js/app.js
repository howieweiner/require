require(['js/zoo'], function(zoo) {
 'use strict';

  var app = {
    init: function() {
      var btn = document.getElementById('visitBtn'),
          console = document.getElementById('console');

      btn.addEventListener('click', function() {
        var animals = zoo.visit(),
            text = '';

        animals.forEach( function(animal) {
          text += '<p>' + animal + '</p>';
        });

        console.innerHTML = text;
      });
    }
  };

  app.init();
});