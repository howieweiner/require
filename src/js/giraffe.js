define(function() {
 'use strict';

  var speak = function() {
    return "Hello. I'm a Giraffe";
  };

  return {
    speak: speak
  }
});