define(['js/giraffe', 'js/zebra'], function (giraffe, zebra) {
  'use strict';

  var visit = function () {
    var animals = [];
    animals.push(giraffe.speak());
    animals.push(zebra.speak());
    return animals;
  };

  return {
    visit: visit
  }
});